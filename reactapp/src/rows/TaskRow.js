import React, {Fragment} from 'react';
import { Table } from 'reactstrap';
import { Container, Row, Col } from 'reactstrap';
import { Button } from 'reactstrap';


const TaskRow = (props) => {
  return (
  	<Fragment>
  		<tr>
          <th scope="row">1</th>
          <td>5e380214e1012d13cce857c8</td>
          <td>Eat now!</td>
          <td><Button size="sm" color="primary"><i className="fas fa-eye"></i></Button>
          <Button size="sm" className="ml-1 mr-1" color="warning"><i className="fas fa-edit"></i></Button>
          <Button size="sm" color="danger"><i className="fas fa-trash-alt"></i></Button></td>
        </tr>
        <tr>
          <th scope="row">2</th>
          <td>5e380214e1012d13cce857c8</td>
          <td>Run now!</td>
          <td><Button size="sm" color="primary"><i className="fas fa-eye"></i></Button>
          <Button size="sm"  className="ml-1 mr-1" color="warning"><i className="fas fa-edit"></i></Button>
          <Button size="sm" color="danger"><i className="fas fa-trash-alt"></i></Button></td>
         </tr>
    </Fragment>
	);
}

export default TaskRow;

