import React, {Fragment} from 'react';
import ReactDOM from 'react-dom';
import { BrowserRouter, Route, Switch, Redirect} from 'react-router-dom'
import { Button } from 'reactstrap';
import 'bootstrap/dist/css/bootstrap.min.css';
import "./style.css";

//pages
import AppNavbar from './partials/AppNavbar';
import MembersPage from './pages/MembersPage';
import LandingPage from './pages/LandingPage';
import TeamsPage from './pages/TeamsPage';
import TasksPage from './pages/TasksPage';
import NotFoundPage from './pages/NotFoundPage';
import LoginPage from './pages/LoginPage';
import RegisterPage from './pages/RegisterPage';


const root = document.querySelector("#root")
	//<Fragment>
		//<h1 className="bg-pink">Hello, Batch 44!</h1>
		//<button className="btn btn-danger">bootstrap button</button>
		//<Button color="primary" className="ml-3">Reactstrap Button</Button>
	//</Fragment>

const pageComponent = (
	<BrowserRouter>
		<AppNavbar/>
		<Switch>
			<Route component= {MembersPage} path="/members"/>
			<Route component = {LandingPage} exact path="/"/>
			<Route component = { TeamsPage } exact path="/teams"/>
			<Route component = { TasksPage } exact path="/tasks"/>
			<Route component = { LoginPage } exact path="/login"/>
			<Route component = { RegisterPage } exact path="/register"/>
			<Route component = { NotFoundPage } to="/notfound"/>
		</Switch>
	</BrowserRouter>
)

ReactDOM.render(pageComponent, root)

//react-dom
