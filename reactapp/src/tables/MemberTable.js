import React from 'react';
import { Table } from 'reactstrap';
import MemberRow from '../rows/MemberRow'
import { Container, Row, Col } from 'reactstrap';


const MemberTable = (props) => {
  return (
    <Table hover borderless sie="sm" className="p-4 rounded">
      <thead>
        <tr>
          <th>#</th>
          <th>Username</th>
          <th>Email</th>
          <th>Team</th>
          <th>Position</th>
          <th>Action</th>
        </tr>
      </thead>
        <tbody>
          <MemberRow/>
        </tbody>
    </Table>
  );
}

export default MemberTable;