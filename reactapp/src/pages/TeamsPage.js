//Dependencies
import React from 'react';
import { Container, Row, Col } from 'reactstrap';
import MemberForm from '../forms/MemberForm';
import MemberTable from '../tables/MemberTable';
import TeamForm from '../forms/TeamForm';
import TeamRow from '../rows/TeamRow';
import TeamTable from '../tables/TeamTable';


//COMPONENTS
const TeamsPage = (props) => { //props - arguments
/*anonymous function*/
//JSX
  return (
    <Container>
      <Row className="mb-4">
        <Col>
          <h1>Teams Page</h1>
        </Col>
      </Row>
      <Row>
        <Col md="4" className="border"><TeamForm/></Col>
        <Col><TeamTable/></Col>
      </Row>
    </Container>
  );
}

export default TeamsPage;

//Discussion
//1.) What is a component?
// - function or class.
// - contains props (arguments)