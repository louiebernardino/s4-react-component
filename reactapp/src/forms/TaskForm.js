import React from 'react';
import { Button, Form, FormGroup, Label, Input, FormText } from 'reactstrap';

const TaskForm = (props) => {
  return (
    <Form className="m-3">
      <FormGroup>
        <Label for="taskDescription">Description</Label>
        <Input type="taskDescription" name="taskDescription" id="taskDescription"/>
      </FormGroup>
      <FormGroup>
        <Label for="memberId">Member Id</Label>
        <Input type="memberId" name="memberId" id="memberId"/>
      </FormGroup>
      <Button color="primary" size="sm" block>Save Changes</Button>
    </Form>
  );
}

export default TaskForm;