import React, { useState, useEffect, Fragment } from 'react';
import { Button, Form, FormGroup, Label, Input, FormText } from 'reactstrap';

const LoginForm = (props) => {

const [formData, setFormData] = useState({
  username: "",
  password: ""
})

const [disabled, setDisabled] = useState(true)

const { username, email, password} = formData;
const onChangeHandler = e => {
  // console.log(e);
  // console.log(e.target)

  //Backticks - If we want multi-line and string interpolation
  // console.log(`e.target.name: ${e.target.name}`)
  // console.log(`e.target.value: ${e.target.value}`)

  //update the state
  setFormData({
    //spread operator - it copies whatever value
    ...formData,
    [e.target.name] : e.target.value
  })
  // console.log(formData)
}

//USE EFFECT
//React HOOK
//hook that tells the component what to do after render
useEffect(()=>{  
if(username !== "" && password !== ""){
  setDisabled(false)
} else {
  setDisabled(true)
}
}, [formData])



  return (
    <Form className="m-3">
      <FormGroup>
        <Label for="username">Username</Label>
        <Input type="username" name="username" id="username" value={username} onChange= {e => onChangeHandler(e)}/>
      </FormGroup>
      <FormGroup>
        <Label for="password">Password</Label>
        <Input type="password" name="password" id="password" value={password} onChange= {e => onChangeHandler(e)}/>
      </FormGroup>
      <Button color="primary" size="sm" block disabled={disabled}>Login</Button>
    </Form>
  );

}


export default LoginForm;