import React, { useState } from 'react';
import { Link } from 'react-router-dom';
import {
  Collapse,
  Navbar,
  NavbarToggler,
  NavbarBrand,
  Nav,
  NavItem,
  NavLink,
  UncontrolledDropdown,
  DropdownToggle,
  DropdownMenu,
  DropdownItem,
  NavbarText
} from 'reactstrap';
import { Button } from 'reactstrap';

const AppNavbar = (props) => {
  const [isOpen, setIsOpen] = useState(false);

  const toggle = () => setIsOpen(!isOpen);

  return (
    <div className="mb-5">
      <Navbar color="secondary" light expand="md">
        <NavbarBrand className="font-weight-bold text-white" href="/">MERN TRACKER</NavbarBrand>
        <NavbarToggler onClick={toggle} />
        <Collapse isOpen={isOpen} navbar>
          <Nav className="mr-auto" navbar>
            <NavItem className="text-black">
              {/*<NavLink href="/components/">Members</NavLink>*/}
              <Link to="/members" className="nav-link">Members</Link>
            </NavItem>
            <NavItem>
              {/*<NavLink href="https://github.com/reactstrap/reactstrap">Teams</NavLink>*/}
              <Link to="/teams" className="nav-link">Teams</Link>
            </NavItem>
            <NavItem>
              {/*<NavLink href="https://github.com/reactstrap/reactstrap">Tasks</NavLink>*/}
              <Link to="/tasks" className="nav-link">Tasks</Link>              
            </NavItem>
          </Nav>
          <Nav>
          <NavItem><Link to="/login" className="nav-link">Login</Link></NavItem>
          <NavItem><NavLink size="sm">Profile</NavLink></NavItem>
          <NavItem><Button color="secondary" size="sm">Logout</Button></NavItem>
          </Nav>
        </Collapse>
      </Navbar>
    </div>
  );
}

export default AppNavbar;